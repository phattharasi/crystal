package com.phattharasi.crystal.gotogeter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by phatt on 6/7/2559.
 */
public class KnowledgeListAdapter extends BaseAdapter {

    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Knowledge> knowledges;

    public KnowledgeListAdapter(Activity activity, ArrayList<Knowledge> knowledges) {
        this.activity = activity;
        this.knowledges = knowledges;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return knowledges.size();
    }

    @Override
    public Knowledge getItem(int position) {
        return this.knowledges.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.knowledges.get(position).getIdKnow());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        view = layoutInflater.inflate(R.layout.listviwe_know, null);
        TextView nameKnow = (TextView) view.findViewById(R.id.nameKnow);
        TextView countyKnow = (TextView) view.findViewById(R.id.countyKnow);

        nameKnow.setText(this.knowledges.get(position).getNameKnow());
        countyKnow.setText(this.knowledges.get(position).getCounty());

        return view;
    }
}
