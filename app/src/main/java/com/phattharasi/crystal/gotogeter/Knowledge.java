package com.phattharasi.crystal.gotogeter;

/**
 * Created by phatt on 4/7/2559.
 */
public class Knowledge {
    private String idKnow;
    private String nameKnow;
    private String county;
    private String idRegion;
    private String nameRegion;
    private String img;
    private String content;

    public String getIdKnow() {
        return idKnow;
    }

    public void setIdKnow(String idKnow) {
        this.idKnow = idKnow;
    }

    public String getNameKnow() {
        return nameKnow;
    }

    public void setNameKnow(String nameKnow) {
        this.nameKnow = nameKnow;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getNameRegion() {
        return nameRegion;
    }

    public void setNameRegion(String nameRegion) {
        this.nameRegion = nameRegion;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
