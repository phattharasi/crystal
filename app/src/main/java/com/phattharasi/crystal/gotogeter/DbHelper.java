package com.phattharasi.crystal.gotogeter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by phatt on 4/7/2559.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String databaseName = "gotogeterdb.sqlite";
    private static final int databaseVersion = 1;
    private Context myContext;

    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //  ตารางภูมิภาค
        String SQLText = "CREATE TABLE region(" +
                "id_region TEXT PRIMARY KEY," +
                "name_region TEXT);";
        db.execSQL(SQLText);

//        ตารางเก็บข้อมูลสถานที่
        SQLText = "CREATE TABLE knowledge(" +
                "id_know TEXT PRIMARY KEY," +
                "name_know TEXT," +
                "county TEXT," +
                "id_region TEXT," +
                "name_region TEXT," +
                "img TEXT," +
                "content TEXT);";
        db.execSQL(SQLText);

//        ใส่ข้อมูลสถานที่
        SQLText = "INSERT INTO knowledge (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('001', 'พระบรมธาตุดอยสุเทพ', 'จังหวัดเชียงใหม่', 'R1', 'ภาคเหนือ', 'img1', 'ddd');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO knowledge (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('002', 'ดอยแม่สลอง', 'จังหวัดเชียงราย', 'R1', 'ภาคเหนือ', 'img2', 'kkk');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO knowledge (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('003', 'ภูชี้ฟ้า', 'จังหวัดเชียงราย', 'R1', 'ภาคเหนือ', 'img3', 'jj');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO knowledge (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('004', 'ดอยกาดผี', 'จังหวัดเชียงราย', 'R1', 'ภาคเหนือ', 'img4', 'jj');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO knowledge (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('005', 'วัดภูมินทร์', 'จังหวัดน่าน', 'R1', 'ภาคเหนือ', 'img5', 'jj');";
        db.execSQL(SQLText);

//        ข้อมูลภูมิภาค
        SQLText = "INSERT INTO region (id_region, name_region)" +
                "VALUES('R1', 'ภาคเหนือ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO region (id_region, name_region)" +
                "VALUES('R2', 'ภาคกลาง');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO region (id_region, name_region)" +
                "VALUES('R3', 'ภาคอีสาน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO region (id_region, name_region)" +
                "VALUES('R4', 'ภาคใต้');";
        db.execSQL(SQLText);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
