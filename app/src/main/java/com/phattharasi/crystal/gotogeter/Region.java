package com.phattharasi.crystal.gotogeter;

/**
 * Created by phatt on 4/7/2559.
 */
public class Region {
    private String idRegion;
    private String nameRegion;

    public String getIdRegion() {
        return idRegion;
    }

    public void setIdRegion(String idRegion) {
        this.idRegion = idRegion;
    }

    public String getNameRegion() {
        return nameRegion;
    }

    public void setNameRegion(String nameRegion) {
        this.nameRegion = nameRegion;
    }
}
